#include <iostream>
#include <fstream>
#include <vector>
#include <random>
#include <string>
#include <ctime>

using std::cin;
using std::cout;
using std::cerr;
using std::clog;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::ios;
using std::string;

void Selector1();
void Selector2();
void Selector3();
bool PrimeNumSelector(unsigned int i);
unsigned int RandomNum();
bool PubKeySelector(unsigned int RandomNum, unsigned int EulerFunction);

int main()
{
	Start1:
	short * Selector = new short; //创建整形指针，用于作出选择
	cout << "请选择你的操作\n1.生成密钥 2.加密 3.解密 其他：退出程序\n未生成密钥或已删除密钥时请先生成密钥" << endl;
	cin >> *Selector;
	switch (*Selector) //多选
	{
		case 1: delete Selector; Selector1(); goto Start1;
		case 2: delete Selector; Selector2(); goto Start1;
		case 3: delete Selector; Selector3(); goto Start1;
		default:
		{
			delete Selector;
			cerr << "ERROR: 无法识别，正在退出程序..." << endl;
			goto Stop; //如无法识别，则跳转到结束
		}
	}
	Stop:
	return 0;
}

void Selector1()
{
	unsigned int * Num = new unsigned int; //创建无符号整形，扩大正数范围
	Start2:
	cout << "请输入一个大于2的数：";
	cin >> *Num;
	if (*Num <= 2)
	{
		cerr << "该数字不合理，请重新输入。" << endl;
		goto Start2; //跳转到开头，重新输入
	}
	unsigned int * PrimeNum = new unsigned int [*Num]; //创建动态质数数组
	for (unsigned int i = 2, a = 0; a < *Num; i++)
	{
		if (PrimeNumSelector(i) == true)
		{
			PrimeNum[a] = i;
			a++;
		} //将质数存放到 PrimeNum 数组
	}
	unsigned int p, q; //选取随机质数
	do
	{
		p = PrimeNum[RandomNum() % *Num];
		q = PrimeNum[RandomNum() % *Num];
	}
	while (p == q);
	unsigned int PrimeProduct = p * q; //质数相乘
	unsigned int EulerFunction = (p - 1) * (q - 1); //欧拉公式
	unsigned int PubKey;
	for (unsigned int RandomTemp; PubKey = 0; RandomTemp = PrimeNum[RandomNum() % *Num];) //从质数数组中选出适合的质数作为公钥
	{PubKey = (PubKeySelector(RandomTemp, EulerFunction) == true) ? RandomTemp : 0;}
	delete [] PrimeNum;
	delete Num;
	unsigned int PriKey;
	do //算出私钥
	{PriKey++;}
	while ((PriKey * PubKey) % EulerFunction != 1);
	//将密钥输出到文件
	ofstream PubKeyFile;
	PubKeyFile.open("PubKey", ios::out | ios::trunc);
	PubKeyFile << PubKey << ' ' << PrimeProduct;
	PubKeyFile.close();
	ofstream PriKeyFile;
	PriKeyFile.open("PriKey", ios::out | ios::trunc);
	PriKeyFile << PriKey << ' ' << PrimeProduct;
	PriKeyFile.close();
	clog << "公钥：" << PubKey << "," << PrimeProduct << endl
		<< "公钥(PUBKEY)与私钥(PRIKEY)存储在该程序所在文件夹" << endl;
}

void Selector2()
{
	using std::vector;
	//将公钥输入变量
	string PubKeyFileName;
	cout << "请输入公钥文件名：";
	do
	{getline(cin, PubKeyFileName);}
	while (PubKeyFileName.empty());
	ifstream PubKeyFile;
	PubKeyFile.open(PubKeyFileName, ios::in);
	unsigned int PubKey, PrimeProduct;
	PubKeyFile >> PubKey >> PrimeProduct;
	PubKeyFile.close();
	cout << "请输入密文（只能输入ASCII编码字符，只允许输入一行，以Enter键为行界限）" << endl
		<< "请开始输入" << endl;
	string PlainText;
	getline(cin, PlainText);
	vector<unsigned int>CipherTextVector; //创建用于存放密文的动态数组
	for (unsigned int i = 0; (i < PlainText.size()); i++) //将每个(ASCII编码的)字符取出，转换为无符号整形，如遇到0(没有字符)或10("\0")则不再继续读取
	{CipherTextVector.push_back((unsigned int)PlainText[i] * PubKey % PrimeProduct);} //将每个字符加密后加入动态数组
	//将密文输出为文件
	ofstream CipherFile;
	CipherFile.open("CipherText", ios::out | ios::trunc);
	for (unsigned int i = 0; i < CipherTextVector.size(); i++)
	{CipherFile << CipherTextVector[i] << ' ';}
	CipherFile << '0';
	CipherFile.close();
	clog << "密文文件(CipherText)存储在该程序所在文件夹" << endl;
}

void Selector3()
{
	string PriKeyFileName;
	cout << "请输入私钥文件名：";
	do
	{getline(cin, PriKeyFileName);}
	while (PriKeyFileName.empty());
	ifstream PriKeyFile;
	PriKeyFile.open(PriKeyFileName, ios::in);
	unsigned int PriKey, PrimeProduct;
	PriKeyFile >> PriKey >> PrimeProduct; //将密钥输入变量
	PriKeyFile.close();
	string CipherTextFileName;
	cout << "请输入存放密文的文件名：";
	do
	{getline(cin, CipherTextFileName);}
	while (CipherTextFileName.empty());
	ifstream CipherFile;
	CipherFile.open(CipherTextFileName, ios::in);
	while (true) //循环读取和解密字符并输出到屏幕
	{
		unsigned int  CipherText;
		CipherFile >> CipherText;
		if (CipherText == 0) //当读取到结尾字符0时打破循环
		{break;}
		unsigned int PlainText = (unsigned int)CipherText * PriKey % PrimeProduct;
		cout << (char)PlainText;
	}
	cout << endl;
	CipherFile.close();
}

bool PrimeNumSelector(unsigned int i)
{

	bool A = true; //刚开始A为true
	for (unsigned int a = 2; a <= i; a++) //两个for循环分别作为两个因数
	{
		for (unsigned int b = 2; a * b <= i; b++)
		{
			if (a * b == i) //相乘后若值与输入的i相等则i有除1和i本身之外的其他因数，则该i值不是质数
			{A = false;} //将a变量赋值为false
		}
	}
	return A;
}

bool PubKeySelector(unsigned int RandomNum, unsigned int EulerFunction)
{
	bool A = true;
	if (RandomNum >= EulerFunction)
	{A = false;}
	for (unsigned int b = 0; b < EulerFunction; b++)
	{
		if (RandomNum * b == EulerFunction)
		{A = false;}
	}
	return A;
}

unsigned int RandomNum()
{
	using std::clock_t;
	clock_t RandSeed = clock();
	srand(RandSeed);
	return (unsigned int)rand();
}
